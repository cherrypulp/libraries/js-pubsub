const channels = new Map();

/**
 * Publish.
 * @param {String} name
 * @param {*} data
 */
export function emit(name, ...data) {
    const stack = channels.get(name);

    if (stack) {
        stack.forEach((callback) => {
            callback(...data);
        });
    }
}

/**
 * Unsubscribe.
 * @param {String} name
 * @param {Function} callback
 */
export function off(name, callback) {
    const stack = channels.get(name);

    if (stack) {
        stack.delete(callback);
        channels.set(name, stack);
    }
}

/**
 * Subscribe.
 * @param {String} name
 * @param {Function} callback
 */
export function on(name, callback) {
    const stack = channels.get(name) || new Set();
    stack.add(callback);
    channels.set(name, stack);
}

/**
 * Subscribe and unsubscribe when emitted.
 * @param {String} name
 * @param {Function} callback
 */
export function once(name, callback) {
    const cb = (...data) => {
        callback(...data);
        off(name, cb);
    };

    on(name, cb);
}
