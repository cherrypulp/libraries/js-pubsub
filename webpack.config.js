const webpack = require('webpack');

module.exports = {
    devServer: {
        contentBase: './src',
        stats: {
            modules: false,
            cached: false,
            colors: true,
            chunk: false,
        },
    },
    devtool: 'source-map',
    entry: './src/pubsub.js',
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(js)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
        ],
    },
    output: {
        path: __dirname + '/dist',
        filename: 'pubsub.js',
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(true),
    ],
    resolve: {
        extensions: ['*', '.js'],
    },
    stats: {
        colors: true,
    },
    target:    'web',
};
