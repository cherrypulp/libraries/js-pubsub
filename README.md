# PubSub

[![codebeat badge](https://codebeat.co/badges/41995228-1de7-4210-a18a-9037e1b0deac)](https://codebeat.co/projects/gitlab-com-cherrypulp-libraries-js-pubsub-master)
[![Issues](https://img.shields.io/badge/issues-0-brightgreen)](https://gitlab.com/cherrypulp/libraries/js-pubsub/issues)
[![License](https://img.shields.io/npm/l/@cherrypulp/types)](https://gitlab.com/cherrypulp/libraries/js-pubsub/blob/master/LICENSE)
[![npm version](https://badge.fury.io/js/%40cherrypulp%2Fpubsub.svg)](https://badge.fury.io/js/%40cherrypulp%2Fpubsub)

PubSub helper.


## Installation

`npm install @cherrypulp/pubsub`


## Quick start

```javascript
import * as event from '@cherrypulp/pubsub';

// or

import {emit, off, on, once} from '@cherrypulp/pubsub';
```

### Methods

#### `emit`

Publish to the given channel.

```javascript
emit('channel', args);
```

#### `off`

Remove the given subscriber.

```javascript
off('channel');
```

#### `on`

Subscribe to a new or existing channel.

```javascript
on('channel', () => console.log('foo'));
```

#### `once`

Subscribe to a new or existing channel and remove the subscriber when published.

```javascript
once('channel', () => console.log('bar'));
```

## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/libraries/js-pubsub/blob/master/LICENSE) file for details.
