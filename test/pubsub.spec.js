import * as event from '../src/pubsub';
import assert from 'assert';
import chai from 'chai';

const expect = chai.expect;
const should = chai.should;

describe('pubsub.js', () => {
    describe('emit', () => {
        it('trigger a callback of an event', () => {
            let fired = false;

            event.on('foo', () => {
                fired = true;
                expect(fired).to.be.true;
            });
            event.emit('foo');
        });

        it('trigger a callback of an event with given argument(s)', () => {
            event.on('bar', (bar) => {
                expect(bar).to.equal('ok');
            });
            event.emit('bar', 'ok');

            event.on('bar2', (bar, baz) => {
                expect(bar).to.equal('ok');
                expect(baz).to.equal('ok2');
            });
            event.emit('bar2', 'ok', 'ok2');
        });

        it('trigger all callbacks of an event', () => {
            let fired = 0;

            event.on('baz', () => {
                expect(fired).to.equal(0);
                fired++;
            });

            event.on('baz', () => {
                expect(fired).to.equal(1);
                fired++;
            });

            event.on('baz', () => {
                expect(fired).to.equal(2);
            });
            event.emit('baz');
        });


        it('trigger all callbacks of an event with argument(s)', () => {
            event.on('faz', (faz) => {
                expect(faz).to.equal('ok');
            });

            event.on('faz', (faz, faz2) => {
                expect(faz).to.equal('ok');
                expect(faz2).to.equal('ok2');
            });

            event.on('faz', (faz, faz2) => {
                expect(faz).to.equal('ok');
                expect(faz2).to.equal('ok2');
            });
            event.emit('faz', 'ok', 'ok2');
        });
    });

    describe('off', () => {
        it('remove an event', () => {
            //expect(types.isAffirmative(typesList.booleanTrue)).to.be.true;
        });
    });

    describe('on', () => {
        it('add an event', () => {
            //expect(types.isAffirmative(typesList.booleanTrue)).to.be.true;
        });
    });

    describe('once', () => {
        it('add en event and remove it', () => {
            //expect(types.isAffirmative(typesList.booleanTrue)).to.be.true;
        });
    });
});
